defmodule Phoenixapp.PageController do
  use Phoenixapp.Web, :controller

  def index(conn, _params) do
    #System.cmd("./rebar", ["compile"], cd: "../erlapp")
    render conn, "index.html"
  end
end
