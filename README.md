# Rebar
  1. github: https://github.com/rebar/rebar
  2. copy rebar to /bin/

# Phoenixapp

To start your Phoenix app in dev:

  1. cd phoenixapp
  2. Install dependencies with `mix deps.get`
  3. Create and migrate your database with `mix ecto.create && mix ecto.migrate`
  4. Start Phoenix endpoint with `iex -S mix phoenix.server`
  5. In the console: System.cmd("rebar", ["compile"], cd: "../erlapp") it works and it will return `{"==> erlapp (compile)\n", 0}`

To start your phoenix app in exrm:

  1. According tohttp://www.phoenixframework.org/docs/advanced-deployment 
  2. Start Phoenix with `rel/phoenixapp/bin/phoenixapp console`
  3. In the console: System.cmd("rebar", ["compile"], cd: "../erlapp") it don't work and it will return `{"", 1}`

Ready to run in production? Please [check our deployment guides](http://www.phoenixframework.org/docs/deployment).

## Learn more

  * Official website: http://www.phoenixframework.org/
  * Guides: http://phoenixframework.org/docs/overview
  * Docs: http://hexdocs.pm/phoenix
  * Mailing list: http://groups.google.com/group/phoenix-talk
  * Source: https://github.com/phoenixframework/phoenix
