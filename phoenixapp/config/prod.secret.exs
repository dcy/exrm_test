use Mix.Config

# In this file, we keep production configuration that
# you likely want to automate and keep it away from
# your version control system.
config :phoenixapp, Phoenixapp.Endpoint,
  secret_key_base: "qyRDP3I1OdxHZ/NgwLvX528kM48TcTvSehTZkgo1GocgvJYtXWb9lzptDygt8IHL"

# Configure your database
config :phoenixapp, Phoenixapp.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: "postgres",
  password: "postgres",
  database: "phoenixapp_prod",
  pool_size: 20
